﻿// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
//           __  __     ____      _____ __                                     :
//          / / / /__  / / /___ _/ ___// /_____  _________ ___                 :
//         / /_/ / _ \/ / / __ `/\__ \/ __/ __ \/ ___/ __ `__ \                :
//        / __  /  __/ / / /_/ /___/ / /_/ /_/ / /  / / / / / /                :
//       /_/ /_/\___/_/_/\__,_//____/\__/\____/_/  /_/ /_/ /_/                 :
//                                                                             :
//              (c) Copyright 2015-2019 HellaStorm, Inc.                       :
//                                                                             :
// Permission is hereby granted, free of charge, to any person obtaining a     :
// copy of this software and associated documentation files (the "Software"),  :
// to deal in the Software without restriction, including without limitation   :
// the rights to use, copy, modify, merge, publish, distribute, sublicense,    :
// and/or sell copies of the Software, and to permit persons to whom the       :
// Software is furnished to do so, subject to the following conditions:        :
//                                                                             :
// The above copyright notice and this permission notice shall be included in  :
// all copies or substantial portions of the Software.                         :
//                                                                             :
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  :
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    :
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     :
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  :
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     :
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         :
// DEALINGS IN THE SOFTWARE.                                                   :
//                                                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : HellaStorm, Inc.                                       :
// Application        : HellaStorm Interface Object                            :
// Filename           : hs_interface.cs                                        :
// Date Created       : 2019 Sep 30                                            :
// Purpose            : Interface functions for accessing HellaStorm base      :
//                      applications.                                          :
// Author(s)          : William Ortega                                         :
// Email              : william.ortega@hellastorm.com                          :
//                                                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
// -----------------------------------------------------------------------------

using System;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;

// Reference NATS client
using NATS.Client;

// HellaStorm Library
namespace HsLib
{
    // Nats.io Stream Event
    public struct FragmentEvent
    {
        [JsonProperty(PropertyName = "event")]
        public String Event;    // Event type: "new_fragment"
        [JsonProperty(PropertyName = "fragment")]
        public String Fragment; // Name of the fragment.  Used to construct the URI for Blob Storage access.
        [JsonProperty(PropertyName = "size")]
        public int Size;        // Size of the fragment in bytes.
        [JsonProperty(PropertyName = "tick")]
        public Int64 Tick;      // Tick value at the time the fragment event was sent.  Used for synchronizing.
    }

    // Callback Delegate Type
    public delegate void StreamEventCallback(FragmentEvent fragEvent);

    // HTTP Request register response.
    public struct RegisterRsp
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success;    // True if the register request succeeded.
        [JsonProperty(PropertyName = "token")]
        public string Token;    // Returned registration token.  Used for all subsequent HsInterface function calls.
        [JsonProperty(PropertyName = "registration_server")]
        public string RegistrationServer;   // The Nats.io server to use for publishing all subsequent HsInterface function requests.
        [JsonProperty(PropertyName = "reason")]
        public string Reason;   // If the register request failed, this holds the reason for the failure.
    }

    // Nats.io Requests/Responses

    // SubscribeStream structures
    public struct SubscribeReq
    {
        [JsonProperty(PropertyName = "stream_id")]
        public string StreamID; // The stream ID to subscribe to.
        [JsonProperty(PropertyName = "token")]
        public string Token;    // The token returned from the http register request.

        // Validates whether the struct has been setup correctly.  True is returned if so.
        public bool Validate()
        {
            // Stream ID field must exist and not be empty.
            if ((this.StreamID == null) || (this.StreamID == ""))
            {
                return false;
            }

            // Token field must be filled with a value.  null or empty strings are not valid.
            if ((this.Token == null) || (this.Token == ""))
            {
                return false;
            }

            return true;
        }
    }

    public struct BlobServer
    {
        [JsonProperty(PropertyName = "server_id")]
        public string ServerId;         // The HellaStorm identifier for this server.
        [JsonProperty(PropertyName = "account_type")]
        public string AccountType;      // The type of account for this server.  One of "hs", "azure", or "aws"
        [JsonProperty(PropertyName = "blob_uri")]
        public string BlobURI;          // The base URI to use for GETing fragments from.
        [JsonProperty(PropertyName = "nats_server")]
        public string NatsServer;       // The Nats.io server to publish requests to.

        public const string AccountHs = "hs";
        public const string AccountAzure = "azure";
        public const string AccountAws = "aws";
    }


    public struct SubscribeRsp
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success;                // True if the subscribe request succeeded.
        [JsonProperty(PropertyName = "reason")]
        public string Reason;               // If the subscribe request failed, this holds the reason for the failure.
        [JsonProperty(PropertyName = "subscription_id")]
        public string SubscriptionID;       // The subscription ID to use when sending an unsubscribe request.
        [JsonProperty(PropertyName = "blob_servers")]
        public BlobServer[] BlobServers;    // List of available servers.  First server in list is the preferred one.
        [JsonProperty(PropertyName = "key")]
        public string Key;                  // For future use.
        [JsonProperty(PropertyName = "key_rotation_interval")]
        public int KeyRotationInterval;     // For future use.
    }

    // UnsubscribeStream structures
    public struct UnsubscribeReq
    {
        [JsonProperty(PropertyName = "subscription_id")]
        public string SubscriptionID;  // The subscription ID to unsubscribe.
        [JsonProperty(PropertyName = "token")]
        public string Token;        // The token returned from the http register request.

        // Validates whether the struct has been setup correctly.  True is returned if so.
        public bool Validate()
        {
            // Subscription IDs can not be 0.
            if (this.SubscriptionID == "")
            {
                return false;
            }

            // Token field must be filled with a value.  null or empty strings are not valid.
            if ((this.Token == null) || (this.Token == ""))
            {
                return false;
            }

            return true;
        }
    }

    public struct UnsubscribeRsp
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success;    // True if the unsubscribe request succeeded.
        [JsonProperty(PropertyName = "reason")]
        public string Reason;   // If the unsubscribe request failed, this holds the reason for the failure.
    }

    public struct EncodingInfo
    {
        [JsonProperty(PropertyName = "name")]
        public string Name;                 // Name of this encoding.
        [JsonProperty(PropertyName = "bitrate")]
        public int BitRate;                 // Bit rate of the encoding in bits per second.
        [JsonProperty(PropertyName = "width")]
        public int Width;                   // Width of the encoding in pixels.
        [JsonProperty(PropertyName = "height")]
        public int Height;                  // Height of the encoding in pixels.
        [JsonProperty(PropertyName = "frame_rate")]
        public int FrameRate;               // Frame rate of the encoding in frames per second.
        [JsonProperty(PropertyName = "pixel_aspect_ratio")]
        public string PixelAspectRatio;     // Pixel aspect ratio for the encoding.
    }

    // AddStream structures
    public struct AddReq
    {
        [JsonProperty(PropertyName = "stream_id")]
        public string StreamID;         // The stream ID for this new stream.
        [JsonProperty(PropertyName = "name")]
        public string Name;             // The human readable description for this stream.
        [JsonProperty(PropertyName = "collection")]
        public string[] Collection;     // An array of strings for matching this stream from a subsequent list request.
        [JsonProperty(PropertyName = "key")]
        public string Key;              // For future use.
        [JsonProperty(PropertyName = "token")]
        public string Token;            // The token returned from the http register request.
        [JsonProperty(PropertyName = "encoding")]
        public EncodingInfo Encoding;   // The specific encoding information for this stream.

        // Validates whether the struct has been setup correctly.  True is returned if so.
        public bool Validate()
        {
            // Stream ID field must exist and not be empty.
            if ((this.StreamID == null) || (this.StreamID == ""))
            {
                return false;
            }

            // Name field must be filled with a value.  null or empty strings are not valid.
            if ((this.Name == null) || (this.Name == ""))
            {
                return false;
            }

/* TODO: Currently disabled until keys are implemented.
            // Key field must be filled with a value.  null or empty strings are not valid.
            if ((this.Key == null) || (this.Key == ""))
            {
                return false;
            }
*/
            // Token field must be filled with a value.  null or empty strings are not valid.
            if ((this.Token == null) || (this.Token == ""))
            {
                return false;
            }

            return true;
        }
    }

    public struct AddRsp
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success;    // True if the add request succeeded.
        [JsonProperty(PropertyName = "reason")]
        public string Reason;   // If the add request failed, this holds the reason for the failure.
        [JsonProperty(PropertyName = "blob_servers")]
        public BlobServer[] BlobServers;  // The list of blob storage servers to write the stream data to.
    }

    // RemoveStream structures
    public struct RemoveReq
    {
        [JsonProperty(PropertyName = "stream_id")]
        public string StreamID; // The stream ID to remove from the registration application.
        [JsonProperty(PropertyName = "token")]
        public string Token;    // The token returned from the http register request.
        [JsonProperty(PropertyName = "encoding_name")]
        public string EncodingName;   // The specific encoding information for this stream.

        // Validates whether the struct has been setup correctly.  True is returned if so.
        public bool Validate()
        {
            // Stream ID field must exist and not be empty.
            if ((this.StreamID == null) || (this.StreamID == ""))
            {
                return false;
            }

            // Token field must be filled with a value.  null or empty strings are not valid.
            if ((this.Token == null) || (this.Token == ""))
            {
                return false;
            }

            // EncodingName field must be filled with a value.  null or empty strings are not valid.
            if ((this.EncodingName == null) || (this.EncodingName == ""))
            {
                return false;
            }

            return true;
        }
    }

    public struct RemoveRsp
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success;    // True if the remove request succeeded.
        [JsonProperty(PropertyName = "reason")]
        public string Reason;   // If the remove request failed, this holds the reason for the failure.
    }

    // ListStreams structures
    public struct ListReq
    {
        [JsonProperty(PropertyName = "collection")]
        public string[] Collection; // Array of strings for filtering against available streams.  Only streams that match all strings are returned.
        [JsonProperty(PropertyName = "token")]
        public string Token;        // The token returned from the http register request.
    }

    public struct StreamInfo
    {
        [JsonProperty(PropertyName = "stream_id")]
        public string StreamID;             // Stream ID for this set of information.
        [JsonProperty(PropertyName = "name")]
        public string Name;                 // Human readable description for this stream.
        [JsonProperty(PropertyName = "collection")]
        public string[] Collection;         // Array of strings describes the match set for this stream.
        [JsonProperty(PropertyName = "encodings")]
        public EncodingInfo[] Encodings;    // Array of available encodings for this stream.
    }
    public struct ListRsp
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success;            // True if the list request succeeded.
        [JsonProperty(PropertyName = "reason")]
        public string Reason;           // If the list request failed, this holds the reason for the failure.
        [JsonProperty(PropertyName = "streams")]
        public StreamInfo[] Streams;    // Array of available streams and associated information.
    }

    // HellaStorm Interface Class
    // Used to access the registration application.
    public class HsInterface
    {
        public const string Version = "0.5.2";
        
        public static HttpClient client;

        public string RegistrationURI;

        // NATS.io variables
        public string NatsServer;
        private bool NatsConnected;
        private string curToken;
        public ConnectionFactory cf;
        public IConnection nc;
        public Options NatsOpts;

        private struct EventNatsConn
        {
            public ConnectionFactory cf;
            public IConnection nc;
            public Options NatsOpts;
            public IAsyncSubscription s;
        }
        private Dictionary<string, EventNatsConn> natsSubs;

        // Stops the interface, closing a Nats.io connection.
        public void Stop()
        {
            if (NatsConnected)
            {
                nc.Close();
                NatsConnected = false;
            }
        }

        // Registers with the registration application and sets up the interface for issuing Nats.io requests.
        public bool HsRegister(out RegisterRsp rsp)
        {
            bool result = true;

            if (client == null)
            {
                client = new HttpClient();
            }

            try
            {
                string data = "{\"action\":\"register\"}";
                StringContent content = new StringContent(data);
                HttpResponseMessage response = Task.Run(async () => { return await client.PostAsync(RegistrationURI, content); }).Result;
                var contents = Task.Run(async () => { return await response.Content.ReadAsStringAsync(); }).Result;
                Console.WriteLine("Returned response :{0}", contents);
                rsp = JsonConvert.DeserializeObject<RegisterRsp>(contents);
                // Store the Nats.io server for other HsInterface requests.
                NatsServer = rsp.RegistrationServer;
                // The token returned during registration is used in all subsequenct HsInterface class calls.
                curToken = rsp.Token;

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0}", e.Message);
                rsp = JsonConvert.DeserializeObject<RegisterRsp>("{\"success\":false,\"reason\":\"HTTP POST failed\"}");
                result = false;
            }

            return result;
        }

        // NATS.io Event Handlers
        void closedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("[closedEventHandler] " + e.ToString());
        }

        void disconnectedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("[disconnectedEventHandler] " + e.ToString());
        }

        void asyncErrorEventHandler(object sender, ErrEventArgs e)
        {
            Console.WriteLine("[asyncErrorEventHandler] " + e.ToString());
        }

        // NATS.io Support Functions
        void InitConn(string serverUrl)
        {
            cf = new ConnectionFactory();
            NatsOpts = ConnectionFactory.GetDefaultOptions();
            // Start exception handlers
            NatsOpts.ClosedEventHandler = closedEventHandler;
            NatsOpts.DisconnectedEventHandler = disconnectedEventHandler;
            NatsOpts.AsyncErrorEventHandler = asyncErrorEventHandler;

            // Extra diagnostic output.
            NatsOpts.Verbose = true;
            //Console.WriteLine("nats server url = {0}", "nats://" + serverUrl);
            //NatsOpts.Url = "nats://" + serverUrl;
            Console.WriteLine("nats server url = {0}", serverUrl);
            NatsOpts.Url = serverUrl;

            NatsOpts.Timeout = 1000;

            nc = cf.CreateConnection(NatsOpts);
            NatsConnected = true;
            if (natsSubs == null)
            {
                natsSubs = new Dictionary<string, EventNatsConn>();
            }
        }

        // NATS.io Interface API

        // Subscribes to a stream ID and installs a callback that is notified whenever a new fragment event is received.
        public bool HsSubscribeStream(string streamId, StreamEventCallback cb, out SubscribeRsp rsp)
        {
            if (!NatsConnected)
            {
                InitConn(NatsServer);
            }

            bool result = true;
            if (curToken != "")
            {
                SubscribeReq req = new SubscribeReq { StreamID = streamId, Token = curToken };
                var jsonData = JsonConvert.SerializeObject(req);
                Console.WriteLine("jsonData : {0}", jsonData);
                var data = Encoding.UTF8.GetBytes(jsonData);
                try
                {
                    Msg m = nc.Request("hellastorm.registration_app.subscribe", data, 20000);
                    rsp = JsonConvert.DeserializeObject<SubscribeRsp>(Encoding.UTF8.GetString(m.Data));
                    Console.WriteLine("rsp data : {0}", rsp);

                    if (!rsp.Success)
                    {
                        // Subscribe failed with a reason code so return it.
                        return false;
                    }

                    EventNatsConn value;
                    if (natsSubs.TryGetValue(rsp.SubscriptionID, out value))
                    {
                        // Error: subscription id already existed in the nats subscription dictionary.
                        Console.WriteLine("subscription ID {0} was already present in Nats.io subscription dictionary!", rsp.SubscriptionID);
                        // Unsubscribe since it succeeded but the nats server for the fragments couldn't be connected to.

                        rsp = JsonConvert.DeserializeObject<SubscribeRsp>("{\"success\":false,\"reason\":\"HTTP POST failed\"}");
                        return false;
                    }

                    // TODO: Check out existing connections to see if the nats server already has a connection.
                    EventNatsConn newNatsSub;

                    // Connect to the specified nats server
                    newNatsSub.cf = new ConnectionFactory();
                    newNatsSub.NatsOpts = ConnectionFactory.GetDefaultOptions();
                    // Start exception handlers
                    newNatsSub.NatsOpts.ClosedEventHandler = closedEventHandler;
                    newNatsSub.NatsOpts.DisconnectedEventHandler = disconnectedEventHandler;
                    newNatsSub.NatsOpts.AsyncErrorEventHandler = asyncErrorEventHandler;

                    // Extra diagnostic output.
                    newNatsSub.NatsOpts.Verbose = true;

                    // Access the first server in the list automatically.
                    Console.WriteLine("nats server url = {0}", rsp.BlobServers[0].NatsServer);
                    newNatsSub.NatsOpts.Url = rsp.BlobServers[0].NatsServer;

                    newNatsSub.NatsOpts.Timeout = 1000;

                    newNatsSub.nc = newNatsSub.cf.CreateConnection(newNatsSub.NatsOpts);

                    // Now subscribe to the stream and link with the provided callback function.
                    // Setup an event handler to process incoming messages.
                    EventHandler<MsgHandlerEventArgs> eh = (sender, values) =>
                    {
                        // print the message
                        Console.WriteLine(String.Format("Full message: {0}", values.Message));
                        Console.WriteLine(String.Format("Data-only: {0}", System.Text.Encoding.UTF8.GetString(values.Message.Data)));

                        // Get the fragment event json structure from the message.
                        FragmentEvent fragEvent = JsonConvert.DeserializeObject<FragmentEvent>(Encoding.UTF8.GetString(values.Message.Data));
                        if (fragEvent.Event == "new_fragment")
                        {
                            Console.WriteLine("Got a new fragment event, sending to callback");
                            cb(fragEvent);
                        }
                        else
                        {
                            Console.WriteLine("Got unknown event {0}", fragEvent);
                        }
                    };

                    Console.WriteLine("New nats connection made, subscribing to channel <{0}>", "hellastorm.blob_storage.sg.action." + streamId);
                    newNatsSub.s = newNatsSub.nc.SubscribeAsync("hellastorm.blob_storage.sg.action." + streamId, eh);

                    natsSubs[rsp.SubscriptionID] = newNatsSub;
                }
                catch (NATSTimeoutException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0}", e.Message);
                    rsp = JsonConvert.DeserializeObject<SubscribeRsp>("{\"success\":false,\"reason\":\"HTTP POST failed\"}");
                    result = false;
                }
            }
            else
            {
                rsp = JsonConvert.DeserializeObject<SubscribeRsp>("{\"success\":false,\"reason\":\"Interface not registered\"}");
                result = false;
            }

            return result;
        }

        // Unsubscribes from a stream ID.  Afterwards no new fragment events will be received.
        public bool HsUnsubscribeStream(string subscriptionId, out UnsubscribeRsp rsp)
        {
            if (!NatsConnected)
            {
                InitConn(NatsServer);
            }

            bool result = true;
            if (curToken != "")
            {
                UnsubscribeReq req = new UnsubscribeReq { SubscriptionID = subscriptionId, Token = curToken };
                var jsonData = JsonConvert.SerializeObject(req);
                Console.WriteLine("jsonData : {0}", jsonData);
                var data = Encoding.UTF8.GetBytes(jsonData);
                try
                {
                    Msg m = nc.Request("hellastorm.registration_app.unsubscribe", data, 20000);
                    rsp = JsonConvert.DeserializeObject<UnsubscribeRsp>(Encoding.UTF8.GetString(m.Data));
                    Console.WriteLine("rsp data : {0}", rsp);

                    if (rsp.Success)
                    {
                        // Now unsubscribe from the nats server.
                        EventNatsConn value;
                        if (natsSubs.TryGetValue(subscriptionId, out value))
                        {
                            // Unsubscribe from the fragment event channel.
                            Console.WriteLine("Unsubscribing");
                            value.s.Unsubscribe();

                            // Close the Nats.io server connection.
                            Console.WriteLine("Closing Nats.io connection");
                            value.nc.Close();

                            // Remove the Nats connection structure from the dictionary.
                            if (natsSubs.Remove(subscriptionId, out value))
                            {
                                Console.WriteLine("Successfully disconnected from nats fragment events");
                            }
                            else
                            {
                                Console.WriteLine("Removal of nats subscription {0} structure failed!", subscriptionId);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Couldn't find entry for subscription ID {0}", subscriptionId);
                        }
                    }
                }
                catch (NATSTimeoutException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0}", e.Message);
                    rsp = JsonConvert.DeserializeObject<UnsubscribeRsp>("{\"success\":false,\"reason\":\"HTTP POST failed\"}");
                    result = false;
                }
            }
            else
            {
                rsp = JsonConvert.DeserializeObject<UnsubscribeRsp>("{\"success\":false,\"reason\":\"Interface not registered\"}");
                result = false;
            }

            return result;
        }

        // Registers a new stream ID with the registration application.  The response returns back the base URI
        // for the Blob Storage application to which fragments will be POSTed.
        public bool HsAddStream(AddReq req, out AddRsp rsp)
        {
            if (!NatsConnected)
            {
                InitConn(NatsServer);
            }

            // Verify that all required fields of the request are set.
            if (!req.Validate())
            {
                Console.WriteLine("\nAddReq has invalid fields!");
                rsp = JsonConvert.DeserializeObject<AddRsp>("{\"success\":false,\"reason\":\"Invalid Request\"}");
                return false;
            }

            bool result = true;
            if (curToken != "")
            {
                req.Token = curToken;
                var jsonData = JsonConvert.SerializeObject(req);
                Console.WriteLine("jsonData : {0}", jsonData);
                var data = Encoding.UTF8.GetBytes(jsonData);
                try
                {
                    Msg m = nc.Request("hellastorm.registration_app.add", data, 20000);
                    rsp = JsonConvert.DeserializeObject<AddRsp>(Encoding.UTF8.GetString(m.Data));
                    Console.WriteLine("rsp data : {0}", rsp);
                }
                catch (NATSTimeoutException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0}", e.Message);
                    rsp = JsonConvert.DeserializeObject<AddRsp>("{\"success\":false,\"reason\":\"HTTP POST failed\"}");
                    result = false;
                }
            }
            else
            {
                rsp = JsonConvert.DeserializeObject<AddRsp>("{\"success\":false,\"reason\":\"Interface not registered\"}");
                result = false;
            }

            return result;
        }

        // Removes a stream from the registration application.  This must only be called when no new clients are
        // permitted to access a stream, or if a stream added to the registration application has been deleted
        // from Blob Storage.
        public bool HsRemoveStream(string streamId, string encodingName, out RemoveRsp rsp)
        {
            if (!NatsConnected)
            {
                InitConn(NatsServer);
            }

            bool result = true;
            if (curToken != "")
            {
                RemoveReq req = new RemoveReq { StreamID = streamId, Token = curToken, EncodingName = encodingName };
                var jsonData = JsonConvert.SerializeObject(req);
                Console.WriteLine("jsonData : {0}", jsonData);
                var data = Encoding.UTF8.GetBytes(jsonData);
                try
                {
                    Msg m = nc.Request("hellastorm.registration_app.remove", data, 20000);
                    rsp = JsonConvert.DeserializeObject<RemoveRsp>(Encoding.UTF8.GetString(m.Data));
                    Console.WriteLine("rsp data : {0}", rsp);
                }
                catch (NATSTimeoutException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0}", e.Message);
                    rsp = JsonConvert.DeserializeObject<RemoveRsp>("{\"success\":false,\"reason\":\"HTTP POST failed\"}");
                    result = false;
                }
            }
            else
            {
                rsp = JsonConvert.DeserializeObject<RemoveRsp>("{\"success\":false,\"reason\":\"Interface not registered\"}");
                result = false;
            }

            return result;
        }

        // Returns a list of streams which match the collection criteria.  An empty collection will match all
        // available streams.
        public bool HsListStreams(string[] collection, out ListRsp rsp)
        {
            if (!NatsConnected)
            {
                InitConn(NatsServer);
            }

            bool result = true;
            if (curToken != "")
            {
                ListReq req = new ListReq { Collection = collection, Token = curToken };
                var jsonData = JsonConvert.SerializeObject(req);
                Console.WriteLine("req jsonData : {0}", jsonData);
                var data = Encoding.UTF8.GetBytes(jsonData);
                try
                {
                    Msg m = nc.Request("hellastorm.registration_app.list", data, 20000);
                    Console.WriteLine("rsp jsonData : {0}", Encoding.UTF8.GetString(m.Data));
                    rsp = JsonConvert.DeserializeObject<ListRsp>(Encoding.UTF8.GetString(m.Data));
                    Console.WriteLine("rsp data : {0}", rsp);
                }
                catch (NATSTimeoutException e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0}", e.Message);
                    rsp = JsonConvert.DeserializeObject<ListRsp>("{\"success\":false,\"reason\":\"HTTP POST failed\"}");
                    result = false;
                }
            }
            else
            {
                rsp = JsonConvert.DeserializeObject<ListRsp>("{\"success\":false,\"reason\":\"Interface not registered\"}");
                result = false;
            }

            return result;
        }
    }
}
