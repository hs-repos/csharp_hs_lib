﻿// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
//           __  __     ____      _____ __                                     :
//          / / / /__  / / /___ _/ ___// /_____  _________ ___                 :
//         / /_/ / _ \/ / / __ `/\__ \/ __/ __ \/ ___/ __ `__ \                :
//        / __  /  __/ / / /_/ /___/ / /_/ /_/ / /  / / / / / /                :
//       /_/ /_/\___/_/_/\__,_//____/\__/\____/_/  /_/ /_/ /_/                 :
//                                                                             :
//              (c) Copyright 2015-2019 HellaStorm, Inc.                       :
//                                                                             :
// Permission is hereby granted, free of charge, to any person obtaining a     :
// copy of this software and associated documentation files (the "Software"),  :
// to deal in the Software without restriction, including without limitation   :
// the rights to use, copy, modify, merge, publish, distribute, sublicense,    :
// and/or sell copies of the Software, and to permit persons to whom the       :
// Software is furnished to do so, subject to the following conditions:        :
//                                                                             :
// The above copyright notice and this permission notice shall be included in  :
// all copies or substantial portions of the Software.                         :
//                                                                             :
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  :
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    :
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     :
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  :
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     :
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         :
// DEALINGS IN THE SOFTWARE.                                                   :
//                                                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : HellaStorm, Inc.                                       :
// Application        : HellaStorm Sample Client                               :
// Filename           : TestClient.cs                                          :
// Date Created       : 2019 Sep 30                                            :
// Purpose            : Provides an example of how to use the HellaStorm       :
//                      HsLib.HsInterface for accessing the Registration App.  :
// Author(s)          : William Ortega                                         :
// Email              : william.ortega@hellastorm.com                          :
//                                                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
// -----------------------------------------------------------------------------

using System;
using System.Threading;

// HellaStorm Interface
using HsLib;

// Microsoft Azure libraries
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;

namespace TestClient
{
    class Program
    {
        public static string RegistrationURI = "http://10.0.2.84:8040/request";
        public static int SleepTime = 20000;

        static bool StartBlobStorageAccess(string blobStorageURI, out CloudStorageAccount acct, out CloudBlobClient client)
        {
            const bool useAzure = false;
            Console.WriteLine("Azure Test");
            if (useAzure)
            {
                acct = null;
                Console.WriteLine("Creating account");
                // Replace the following key with one for a real account.
                if (!CloudStorageAccount.TryParse("DefaultEndpointsProtocol=http;AccountName=hscloud;AccountKey=xyz==;EndpointSuffix=core.windows.net", out acct))
                {
                    Console.WriteLine("FAILED to create account");
                    return false;
                }
                try
                {
                    client = acct.CreateCloudBlobClient();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception creating Azure client: {0}", ex.Message);
                    return false;
                }
            }
            else
            {
                acct = new CloudStorageAccount(null, new Uri(blobStorageURI), null, null, null);
                client = acct.CreateCloudBlobClient();
            }

            return true;
        }

        public class CloudStreamInfo
        {
            public CloudStorageAccount acct;
            public CloudBlobClient client;
            public string StreamGUID;
            public byte[] ByteArray;
            public CloudBlobContainer Container;
            public int Active;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Starting");

            // Instantiate the interface.
            HsInterface hi = new HsInterface{ RegistrationURI = RegistrationURI };

            // Need to define the cloud storage variables here to use in the event callback.
            CloudStreamInfo csi = new CloudStreamInfo { ByteArray = new byte[1000000], Active = 0 };

            RegisterRsp rsp;
            bool result = hi.HsRegister(out rsp);
            if (result)
            {
                Console.WriteLine("Registration succeeded");

                // Fragment event callback is defined here.  It must be done prior to HsSubscribeStream() call.
                // The anonymous delegate function uses the previously defined CloudStreamInfo structure so that
                // the results of the subscribe stream call can set in the structure and used here.
                StreamEventCallback sec = (fragEvent) =>
                {
                    //Console.WriteLine("Got new fragment event");
                    //Console.WriteLine("  Event {0}", fragEvent.Event);
                    Console.WriteLine("  Fragment {1}  time {2}", fragEvent.Fragment, (DateTime.Now).ToString());
                    //Console.WriteLine("  Size {0}", fragEvent.Size);
                    //Console.WriteLine("  Tick {0}", fragEvent.Tick);

                    if (csi.Active == 1)
                    {
                        // Only start processing if the csi class has been setup.
                        try
                        {
                            //Console.WriteLine("Creating blob container object");
                            //var blob = csi.Container.GetBlockBlobReference(fragEvent.Fragment);

                            //Console.WriteLine("Downloading to byte array");
                            //var num = blob.DownloadToByteArray(csi.ByteArray, 0);

                            //Console.WriteLine("Done");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception during test: {0}", ex);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Got fragment but client not active yet");
                    }
                };

                // Get the list of available streams
                // Set the collection array to hold the match parameters for the stream.  And empty string
                // matches everything.
                ListRsp listRsp;
                string[] collection = { "" };
                result = hi.HsListStreams(collection, out listRsp);

                if (result)
                {
                    if (listRsp.Streams != null)
                    {
                        SubscribeRsp subRsp;

                        // Select the stream from the list, then subscribe to it.  The test client always
                        // chooses the first stream.
                        Console.WriteLine("Attempting to subscribe to {0}", listRsp.Streams[0].StreamID);
                        result = hi.HsSubscribeStream(listRsp.Streams[0].StreamID, sec, out subRsp);

                        if (result)
                        {
                            Console.WriteLine("Subscribe stream succeeded");

                            //Setup for BlobStorage access
                            csi.StreamGUID = listRsp.Streams[0].StreamID;
                            result = StartBlobStorageAccess(subRsp.BlobServers[0].BlobURI, out csi.acct, out csi.client);
                            csi.Container = csi.client.GetContainerReference(csi.StreamGUID);

                            if (result)
                            {
                                // Blob storage access setup.  Activate the callback.
                                Interlocked.Increment(ref csi.Active);

                                Console.WriteLine("Starting at {0}", (DateTime.Now).ToString());
                                // Sleep for sleepTime milliseconds.  Change this for however long the client should
                                // continue to process incoming requests.
                                Console.WriteLine("Sleeping for {0} seconds", SleepTime / 1000);

                                System.Threading.Thread.Sleep(SleepTime);
                                Console.WriteLine("Woke up, shutting down client.");
                            }
                            else
                            {
                                Console.WriteLine("ERROR: Setting blob storage client failed!");
                            }

                            // When the test client is done unsubscribe for stream fragment events.
                            UnsubscribeRsp unsubscribeRsp;
                            result = hi.HsUnsubscribeStream(subRsp.SubscriptionID, out unsubscribeRsp);

                            if (result)
                            {
                                Console.WriteLine("Unsubscribe stream succeeded");
                            }
                            else
                            {
                                Console.WriteLine("ERROR: HsUnsubscribeStream() failed!");
                            }
                        }
                        else
                        {
                            // Subscribe stream command failed
                            Console.WriteLine("ERROR: HsSubscribeStream() failed!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No streams available");
                    }
                }
                else
                {
                    // List streams command failed
                    Console.WriteLine("ERROR: HsListStreams() failed!");
                }

                Console.WriteLine("Woke up, exiting");

                // Stop the interface
                hi.Stop();
            }
            else
            {
                // Registration failed
                Console.WriteLine("ERROR: HsRegister() failed!");
            }

            Console.WriteLine("Stopping");
        }
    }
}