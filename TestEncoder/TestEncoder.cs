﻿// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
//           __  __     ____      _____ __                                     :
//          / / / /__  / / /___ _/ ___// /_____  _________ ___                 :
//         / /_/ / _ \/ / / __ `/\__ \/ __/ __ \/ ___/ __ `__ \                :
//        / __  /  __/ / / /_/ /___/ / /_/ /_/ / /  / / / / / /                :
//       /_/ /_/\___/_/_/\__,_//____/\__/\____/_/  /_/ /_/ /_/                 :
//                                                                             :
//              (c) Copyright 2015-2019 HellaStorm, Inc.                       :
//                                                                             :
// Permission is hereby granted, free of charge, to any person obtaining a     :
// copy of this software and associated documentation files (the "Software"),  :
// to deal in the Software without restriction, including without limitation   :
// the rights to use, copy, modify, merge, publish, distribute, sublicense,    :
// and/or sell copies of the Software, and to permit persons to whom the       :
// Software is furnished to do so, subject to the following conditions:        :
//                                                                             :
// The above copyright notice and this permission notice shall be included in  :
// all copies or substantial portions of the Software.                         :
//                                                                             :
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  :
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    :
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     :
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  :
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     :
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         :
// DEALINGS IN THE SOFTWARE.                                                   :
//                                                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//                                                                             :
// Client             : HellaStorm, Inc.                                       :
// Application        : HellaStorm Sample Encoder                              :
// Filename           : TestEncoder.cs                                         :
// Date Created       : 2019 Sep 30                                            :
// Purpose            : Provides an example of how to use the HellaStorm       :
//                      HsLib.HsInterface for accessing the Registration App.  :
// Author(s)          : William Ortega                                         :
// Email              : william.ortega@hellastorm.com                          :
//                                                                             :
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
// -----------------------------------------------------------------------------

using System;
using System.Threading;
using System.Threading.Tasks;

// HellaStorm Interface
using HsLib;

// Microsoft Azure libraries
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;

// Reference NATS client
using NATS.Client;

namespace TestEncoder
{
    class Program
    {
        public static string RegistrationURI = "http://10.0.2.50:8040/request";
        public static int SleepTime = 20000;

        static bool StartBlobStorageAccess(string blobStorageURI, out CloudStorageAccount acct, out CloudBlobClient client)
        {
            const bool useAzure = false;
            Console.WriteLine("Azure Test");
            if (useAzure)
            {
                acct = null;
                Console.WriteLine("Creating account");
                // Replace the following key with one for a real account.
                if (!CloudStorageAccount.TryParse("DefaultEndpointsProtocol=http;AccountName=hscloud;AccountKey=xyz==;EndpointSuffix=core.windows.net", out acct))
                {
                    Console.WriteLine("FAILED to create account");
                    return false;
                }
                try
                {
                    client = acct.CreateCloudBlobClient();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception creating Azure client: {0}", ex.Message);
                    return false;
                }
            }
            else
            {
                acct = new CloudStorageAccount(null, new Uri(blobStorageURI), null, null, null);
                client = acct.CreateCloudBlobClient();
            }

            return true;
        }

        public class CloudStreamInfo
        {
            public CloudStorageAccount acct;
            public CloudBlobClient client;
            public string StreamGUID;
            public byte[] ByteArray;
            public CloudBlobContainer Container;
            public int Active;
        }

        static void CreateContainer(CloudStreamInfo csi)
        {
            csi.Container = csi.client.GetContainerReference(csi.StreamGUID);
            Console.WriteLine("Container URI={0}", csi.Container.Uri);
            var result = Task.Run(async () => await csi.Container.CreateIfNotExistsAsync()).Result;
        }

        // NATS.io Event Handlers
        static void closedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("[closedEventHandler] " + e.ToString());
        }

        static void disconnectedEventHandler(object sender, ConnEventArgs e)
        {
            Console.WriteLine("[disconnectedEventHandler] " + e.ToString());
        }

        static void asyncErrorEventHandler(object sender, ErrEventArgs e)
        {
            Console.WriteLine("[asyncErrorEventHandler] " + e.ToString());
        }

        
        static void Main(string[] args)
        {
            Console.WriteLine("Starting");

            // Instantiate the interface.
            HsInterface hi = new HsInterface { RegistrationURI = RegistrationURI };

            // Need to define the cloud storage variables here to use in the event callback.
            CloudStreamInfo csi = new CloudStreamInfo { ByteArray = new byte[1000000], Active = 0 };

            RegisterRsp rsp;
            bool result = hi.HsRegister(out rsp);
            if (result)
            {
                Console.WriteLine("Registration succeeded");

                Guid streamGuid = Guid.NewGuid();
                var encoding = new HsLib.EncodingInfo { Name = "500k", BitRate = 500000, Width = 640, Height = 400, FrameRate = 30, PixelAspectRatio = "16x9" };
                AddReq req = new AddReq { StreamID = streamGuid.ToString(), Name = "TestC#Stream", Token = rsp.Token, Encoding = encoding };
                AddRsp addRsp;
                if (hi.HsAddStream(req, out addRsp))
                {
                    if (StartBlobStorageAccess(addRsp.BlobServers[0].BlobURI, out csi.acct, out csi.client))
                    {
                        csi.StreamGUID = streamGuid.ToString();
                        CreateContainer(csi);

                        // Send messages for 30 seconds, one per second.
                        for (int i = 0; i < 30; i++)
                        {
                            string fragment = string.Format("xyz/fragment_{0}_500k.mp3", i);
                            var blob = csi.Container.GetBlockBlobReference(fragment);
                            Console.WriteLine("Blob URI={0}", blob.Uri);
                            blob.UploadText("This is another test");
                            Thread.Sleep(1000);
                        }

                        // Enable the following if containers should be deleted after the test.
                        //Console.WriteLine("Deleting container");
                        //result = Task.Run(async () => await csi.Container.DeleteIfExistsAsync()).Result;
                        //Console.WriteLine("Done with result {}", result);
                    }
                    else
                    {
                        Console.WriteLine("Failed to start blob storage access");
                    }

                    // Remove the following if streams should remain active after the test.
                    Console.WriteLine("Removing stream {0}", streamGuid.ToString());
                    RemoveRsp rmRsp;
                    result = hi.HsRemoveStream(streamGuid.ToString(), "500k", out rmRsp);
                    Console.WriteLine("Done with result {0}", result);
                }
                else
                {
                    // Add stream command failed
                    Console.WriteLine("ERROR: HsAddStream() failed!");
                }
            }
            else
            {
                // Registration failed
                Console.WriteLine("ERROR: HsRegister() failed!");
            }

            Console.WriteLine("Stopping");

            hi.Stop();
        }
    }
}
